//package com.batko;
//
//import java.util.*;
//import java.util.stream.*;
//
//
//public class Fibonacci {
//
//
//    public static void main(String[] args) {
//
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Input number \"from\":");
//        int startNum = sc.nextInt();
//        System.out.println("Input number \"to\":");
//        int endNum = sc.nextInt();
//        int sumOddNum = 0;
//        int sumEvenNum = 0;
//        ArrayList<Integer> oddNums = new ArrayList<Integer>();
//        ArrayList<Integer> evenNums = new ArrayList<Integer>();
//        int maxOddNum;
//        int maxEvenNum;
//
//        for (int i = startNum; i < endNum; i++) {
//            if (i % 2 != 0) {
//                System.out.print(i + " ");
//                sumOddNum += i;
//                oddNums.add(i);
//            }
//        }
//        maxOddNum = Collections.max(oddNums);
//
//        System.out.println("\n" + "Sum of odd numbers: " + sumOddNum);
//        System.out.println("Max odd num: " + maxOddNum);
////        System.out.println("\n");
//
//        for (int j = endNum; j > 0; j--) {
//            if (j % 2 != 1) {
//                System.out.print(j + " ");
//                sumEvenNum += j;
//                evenNums.add(j);
//            }
//        }
//        maxEvenNum = Collections.max(evenNums);
//
//        System.out.println("\n" + "Sum of even numbers: " + sumEvenNum);
//        System.out.println("Max even num: " + maxEvenNum);
//
//
//    }
//
//}
//
//class FibonacciExample2 {
//    static int n1 = 0, n2 = 1, n3 = 0;
//
//    static void printFibonacci(int count) {
//        if (count > 0) {
//            n3 = n1 + n2;
//            n1 = n2;
//            n2 = n3;
//            System.out.print(" " + n3);
//            printFibonacci(count - 1);
//        }
//    }
//
//    public static void main(String args[]) {
//        int count = 10;
//        System.out.print(n1 + " " + n2);//printing 0 and 1
//        printFibonacci(count - 2);//n-2 because 2 numbers are already printed
//    }
//}