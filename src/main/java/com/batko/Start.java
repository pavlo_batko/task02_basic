package com.batko;

import java.util.Scanner;

public class Start {
    public static void main(String[] args) {
        int fibSize;

        EvenOdd evenOddsArray = new EvenOdd();
        evenOddsArray.run();

        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nEnter size of Fibonacci array:");
        fibSize = scanner.nextInt();
        Fibonaccii fibonacci = new Fibonaccii(fibSize, evenOddsArray.getMaxOdd(),
                evenOddsArray.getMaxEven());
        fibonacci.run();
    }
}
